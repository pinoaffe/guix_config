(define extra_packages? #f)
(define own_guix? #f)
(define own_guix_branch "fork")
(define own_guix
        (channel
          (name 'guix)
          (url "https://gitlab.com/pinoaffe/guix_fork.git")
          (branch own_guix_branch)))
(define extra_packages
        (channel
          (name 'personal-packages)
          (url "https://gitlab.com/pinoaffe/guix_config.git")))
(if own_guix?
    (if extra_packages?
        (list extra_packages own_guix)
        (list own_guix))
    (if extra_packages?
        (cons extra_packages %default-channels)
        %default-channels))
