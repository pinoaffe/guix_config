(use-modules (gnu)
             (gnu system nss)
             (srfi srfi-1))
(use-service-modules desktop cups networking ssh xorg virtualization pm audio)
(use-package-modules bootloaders certs suckless wm text-editors shells android cups linux)

(operating-system
 (host-name "brunhildr")
 (timezone "Europe/Amsterdam")
 (locale "en_GB.UTF-8")
 (keyboard-layout
   (keyboard-layout "us" "altgr-intl"))

 (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (target "/dev/sda")
               (keyboard-layout keyboard-layout)))

 ;(swap-devices (list "/dev/sda1"))

 (file-systems (cons* (file-system
                       (device (uuid "2e9b03ea-3bd1-4945-a700-039346b64696" 'ext4))
                       (mount-point "/")
                       (type "ext4"))
                      %base-file-systems))

 (groups (cons* (user-group (name "adbusers"))
                (user-group (name "plugdev"))
                %base-groups))

 (users (cons (user-account
               (name "pino")
               (comment "")
               (group "users")
               (supplementary-groups '("wheel"
                                       "netdev"
                                       "audio"
                                       "video"
                                       "adbusers"
                                       "libvirt"
                                       "plugdev")))
              %base-user-accounts))

 (packages
  (append
   (map specification->package
        (list
         "st"
         "i3-wm"
         "guile-wm"
         "emacs-exwm"
         "emacs"
         "i3status"
         "dmenu"
         "i3lock"
         "slock"
         "nss-certs"
         "zsh"
         "xf86-video-intel"
         "acpid"
         "kakoune"))
   %base-packages))

 (services
  (cons*
   (service openssh-service-type)
   (service tor-service-type)
   (set-xorg-configuration
     (xorg-configuration
       (keyboard-layout keyboard-layout)))
   (service libvirt-service-type
            (libvirt-configuration
             (unix-sock-group "libvirt")
             (tls-port "16555")))
   (service virtlog-service-type
            (virtlog-configuration
             (max-clients 1000)))
   (service qemu-binfmt-service-type
            (qemu-binfmt-configuration
             (platforms (lookup-qemu-platforms
                         "arm"
                         "aarch64"
                         "riscv32"
                         "riscv64"))))
   (service thermald-service-type)
   (service autossh-service-type
            (autossh-configuration
             (user "pino")
             (ssh-options (list "-T" "-N" "-L" "8081:localhost:8081" "sigrdrifa.net"))))
   (service mpd-service-type
            (mpd-configuration
             (user "pino")
             (port "6600")
             (music-dir "/data/music")
             (default-permissions '(read))
             (credentials (list (mpd-credential
                                 (password "test")
                                 (permissions '(read add control admin)))))
             (outputs (list (mpd-output
                             (name "pulseaudio"))
                            (mpd-output
                             (name "streaming")
                             (type "httpd")
                             (mixer-type 'null)
                             (extra-options
                              `((encoder . "vorbis")
                                (port . "8080"))))))))
   (service cups-service-type
            (cups-configuration
             (web-interface? #t)
             (extensions
              (list cups-filters escpr hplip-minimal))))
   (modify-services
    %desktop-services
    (udev-service-type
     config => (udev-configuration
                (inherit config)
                (rules
                 (cons* brightnessctl
                        android-udev-rules
                        (udev-rule "52-edl.rules"
                                    (string-append
                                     "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"05c6\", ATTRS{idProduct}==\"9008\", MODE=\"0666\", GROUP=\"plugdev\"\n"
                                     "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"05c6\", ATTRS{idProduct}==\"9006\", MODE=\"0666\", GROUP=\"plugdev\"\n"
                                     "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"05c6\", ATTRS{idProduct}==\"900e\", MODE=\"0666\", GROUP=\"plugdev\"\n"
                                     "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1004\", ATTRS{idProduct}==\"61a1\", MODE=\"0666\", GROUP=\"plugdev\"\n"
                                     "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1199\", ATTRS{idProduct}==\"9071\", MODE=\"0666\", GROUP=\"plugdev\"\n"))
                         (udev-configuration-rules config))))))))

 (name-service-switch %mdns-host-lookup-nss))
