(use-modules (gnu)
             (gnu system nss)
             (srfi srfi-1))
(use-service-modules desktop cups networking ssh xorg virtualization pm audio)
(use-package-modules bootloaders certs suckless wm text-editors shells android cups)

(operating-system
 (host-name "geirskogul")
 (timezone "Europe/Amsterdam")
 (locale "en_GB.UTF-8")
 (keyboard-layout
   (keyboard-layout "us" "altgr-intl"))

 (bootloader (bootloader-configuration
               (bootloader grub-efi-bootloader)
               (target "/boot/efi")
               (keyboard-layout keyboard-layout)))

 (swap-devices '())

 (file-systems (cons* (file-system
                        (device (uuid "43f088e9-41d2-49bd-9a1d-4025bffd89bc" 'ext4))
                        (mount-point "/")
                        (type "ext4"))
                      %base-file-systems))

 (groups %base-groups)

 (users (cons (user-account
               (name "pino")
               (comment "")
               (group "users")
               (supplementary-groups '("wheel"
                                       "netdev"
                                       "audio"
                                       "video"
                                       "libvirt")))
              %base-user-accounts))

 (packages
  (append
   (map specification->package
        (list
         "st"
         "emacs-exwm"
         "emacs"
         "dmenu"
         "slock"
         "lemonbar"
         "nss-certs"
         "zsh"
         "xf86-video-intel"
         "acpid"
         "kakoune"))
   %base-packages))

 (services
  (cons*
   (service openssh-service-type
            (openssh-configuration
             (password-authentication? #f)
             (permit-root-login 'without-password)
             (authorized-keys `(("root" ,(local-file "../pubkeys/pino_brunhildr.pub"))
                                ("pino" ,(local-file "../pubkeys/pino_brunhildr.pub"))))
             (port-number 2222)))
   (set-xorg-configuration
     (xorg-configuration
       (keyboard-layout keyboard-layout)))
   (service libvirt-service-type
            (libvirt-configuration
             (unix-sock-group "libvirt")
             (tls-port "16555")))
   (service virtlog-service-type
            (virtlog-configuration
             (max-clients 1000)))
   (service qemu-binfmt-service-type
            (qemu-binfmt-configuration
             (platforms (lookup-qemu-platforms
                         "arm"
                         "aarch64"
                         "riscv32"
                         "riscv64"))))
   (screen-locker-service slock "slock")
   ;(service autossh-service-type
   ;         (autossh-configuration
   ;          (user "pino")
   ;          (ssh-options (list "-T" "-N" "-L" "8081:localhost:8081" "sigrdrifa.net"))))
   (service mpd-service-type
            (mpd-configuration
             (user "pino")
             (port "6600")
             (music-dir "/data/music")
             ;(default-permissions '(read))
             ;(credentials (list (mpd-credential
             ;                    (password "test")
             ;                    (permissions '(read add control admin)))))
             (outputs (list (mpd-output
                             (name "pulseaudio"))
                            (mpd-output
                             (name "streaming")
                             (type "httpd")
                             (mixer-type 'null)
                             (extra-options
                              `((encoder . "vorbis")
                                (port . "8080"))))))))
   %desktop-services))

 (name-service-switch %mdns-host-lookup-nss))
