(use-modules (gnu) (gnu system nss) (srfi srfi-1))
(use-service-modules desktop cups networking ssh certbot)
(use-package-modules bootloaders certs suckless text-editors shells)


(operating-system
  (host-name "sigrdrifa")
  (timezone "Europe/Amsterdam")
  (locale "en_GB.UTF-8")
  (keyboard-layout
    (keyboard-layout "us" "altgr-intl"))

  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sda")
                (keyboard-layout keyboard-layout)))
 
  (file-systems (cons (file-system
                         (device (file-system-label "root"))
                         (mount-point "/")
                         (type "ext4"))
                       %base-file-systems))

  (users (append (list (user-account
                        (name "pino")
                        (group "users")
                        (supplementary-groups '("wheel" "netdev" "audio" "video"))
                        (home-directory "/home/pino"))
                       (user-account
                        (name "haiku")
                        (group "users")
                        (home-directory "/home/haiku")
                        (system? #t))
                       (user-account
                        (name "synapse")
                        (group "users")
                        (home-directory "/home/synapse")
                        (system? #t)))
               %base-user-accounts))
  (packages
    (append
      (map specification->package
           (list
             "st"
             "nss-certs"
             "zsh"
             "screen"
             "acpid"
             "kakoune"))
      %base-packages))

  (initrd-modules (append (list "virtio_pci"
                                "virtio_scsi")
                          %base-initrd-modules))

  (services (append (list (service dhcp-client-service-type)
			  (service certbot-service-type
				   (certbot-configuration
				    (email "pinoaffe@airmail.cc")
				    (certificates (list (certificate-configuration
							 (name "sigrdrifa")
							 (domains (list "sigrdrifa.net"
									"www.sigrdrifa.net"
									"matrix.sigrdrifa.net"
									"carddav.sigrdrifa.net"
									"mail.sigrdrifa.net")))
							(certificate-configuration
							 (name "brunhildr")
							 (domains (list "brunhildr.net"
									"www.brunhildr.net"
									"music.brunhildr.net")))))))
                          (service openssh-service-type
                                   (openssh-configuration
                                    (password-authentication? #f)
                                    (permit-root-login 'without-password)
                                    (authorized-keys `(("root" ,(local-file "../pubkeys/pino_brunhildr.pub"))
                                                       ("pino" ,(local-file "../pubkeys/pino_brunhildr.pub"))
                                                       ("haiku" ,(local-file "../pubkeys/pino_brunhildr.pub"))
                                                       ("synapse" ,(local-file "../pubkeys/pino_brunhildr.pub"))))
                                    (port-number 2222))))
                    %base-services))


  (name-service-switch %mdns-host-lookup-nss))
