(specifications->manifest
 '("emacs"
   "emacs-multiple-cursors"
   "emacs-expand-region"
   "emacs-ryo-modal"
   "emacs-kakoune"
   "emacs-undo-tree"
   "emacs-phi-search"
   "emacs-phi-search-mc"
   "emacs-visual-regexp"

   "emacs-emms"
   "emacs-mpdel"
   "emacs-libmpdel"

   "emacs-elfeed"
   "emacs-elfeed-org"

   "emacs-use-package"
   "emacs-diminish"
   "emacs-projectile"
   "emacs-ibuffer-projectile"
   "emacs-dired-hacks"
   "emacs-geiser"
   "emacs-magit"
   "emacs-forge"
   "emacs-parinfer-mode"
   "emacs-helm"
   "git"

   "emacs-material-theme"
   "emacs-modus-themes"
   "emacs-rainbow-delimiters"
   "emacs-spaceline"
   "emacs-rainbow-mode"
   "emacs-all-the-icons"
   "emacs-all-the-icons-dired"
   "emacs-all-the-icons-ibuffer"
   "emacs-spaceline-all-the-icons"
   "emacs-svg-tag-mode"
   "emacs-dimmer"
   "emacs-visual-fill-column"

   "font-google-roboto"
   "font-google-roboto-mono"
   "font-google-noto"
   "font-awesome"

   "emacs-form-feed"
   "emacs-yasnippet"
   "emacs-no-littering"
   "emacs-elpy"
   "emacs-flycheck"

   "emacs-buffer-move"
   "emacs-transpose-frame"
   "emacs-shackle"
   "emacs-exwm"
   "emacs-desktop-environment"
   "emacs-dashboard"
   "emacs-burly"

   "emacs-pinentry"
   "emacs-pass"
   "emacs-password-store"
   "emacs-auth-source-pass"
   "xdotool"

   "emacs-bash-completion"

   "guile"
   "lemonbar"

   "pulsemixer"
   "emacs-pulseaudio-control"

   "brightnessctl"

   "zathura"
   "emacs-pdf-tools"
   "emacs-nov-el"

   "aspell"
   "aspell-dict-en"
   "aspell-dict-uk"
   "aspell-dict-nl"
   "aspell-dict-de"
   "sdcv"

   "setxkbmap"
   "slock"
   "scrot"

   "offlineimap"
   "msmtp"
   "emacs-async"
   "mu"

   "emacs-shrface"
   ;"emacs-org-inherit"
   "emacs-org-mime"
   "emacs-org-web-tools"

   "emacs-edit-server"
   "emacs-exwm-edit"


   "nss-certs"
   "man-db"
   "info-reader"
   "pkg-config"))
