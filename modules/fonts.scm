(define-module (fonts)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build font-build-system)
  #:use-module (guix build-system font)
  #:use-module (gnu packages emacs-xyz))

(define-public font-google-roboto-mono
  (package
   (name "font-google-roboto-mono")
   (version "1.4.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/googlefonts/RobotoMono")
                  (commit "8f651634e746da6df6c2c0be73255721d24f2372")))
            (file-name (git-file-name name version))
            (sha256
             (base32
              "00ddmr7yvb9isakfvgv6g74m80fmg81dmh1hrrdyswapaa7858a5"))))
   (build-system font-build-system)
   (home-page "https://github.com/googlefonts/RobotoMono")
   (synopsis "blarg")
   (description "blarg")
   (license license:asl2.0)))
