(define-module (python-flask-scss)
    #:use-module ((guix licenses) #:prefix license:)
    #:use-module (guix packages)
    #:use-module (guix download)
    #:use-module (guix build-system python)
    #:use-module (gnu packages python)
    #:use-module (gnu packages python-web)
    #:use-module (gnu packages python-xyz)
    #:use-module (gnu packages check))

(define-public python-pyscss
  (package
    (name "python-pyscss")
    (version "1.3.7")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyScss" version))
        (sha256
          (base32
            "1gs68waf061ybxyfmhaxsfz81pjx81ab33jk3aa266h2d4amgpzi"))))
    (build-system python-build-system)
    (propagated-inputs `(("python-six" ,python-six)))
    (arguments '(#:tests? #f))
    (home-page "http://github.com/Kronuz/pyScss")
    (synopsis "pyScss, a Scss compiler for Python")
    (description
      "pyScss, a Scss compiler for Python")
    (license license:expat)))

(define-public python-flask-scss
  (package
    (name "python-flask-scss")
    (version "0.5")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "Flask-Scss" version))
        (sha256
          (base32
            "10j7yxwgsdshfyrlw8b1n3fm1cn0lfj7hm88174hangx0cxqrqi1"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs
      `(("python-flask" ,python-flask)
        ("python-pyscss" ,python-pyscss)))
    (home-page
      "https://github.com/bcarlin/flask-scss")
    (synopsis
      "Adds support for scss files to Flask applications")
    (description
      "Adds support for scss files to Flask applications")
    (license license:expat)))
