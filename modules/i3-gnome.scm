(define-module (i3-gnome)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build gnu-build-system)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages pkg-config)
  #:use-module (srfi srfi-1))

(define-public i3-gnome
  (package
    (name "i3-gnome")
    (version "3.34.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/" name "/" name "/archive/" version ".tar.gz"))
              (sha256
               (base32
                "1jb73mr556v2hhgpi16plqr88c1m6cq0s9fk2aps1l4npx9841m1"))))
    (build-system gnu-build-system)
    (arguments
      `(
        #:tests? #f
        #:make-flags (list (string-append "DESTDIR=" (assoc-ref %outputs "out")))
        #:phases (modify-phases %standard-phases
                                (delete 'configure))))
    (inputs
      `(("i3-wm" ,i3-wm)
        ("gnome" ,gnome)
        ("gdm" ,gdm)
        ;("gnome-flashback" ,gnome-flashback)
       ))
    (home-page (string-append "https://github.com/" name "/" name))
    (description "asdf")
    (synopsis "asdf")
    (license license:expat)))
