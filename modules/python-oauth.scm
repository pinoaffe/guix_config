(define-module (python-oauth)
    #:use-module ((guix licenses) #:prefix license:)
    #:use-module (guix packages)
    #:use-module (guix download)
    #:use-module (guix build-system python)
    #:use-module (gnu packages python)
    #:use-module (gnu packages python-web)
    #:use-module (gnu packages python-xyz)
    #:use-module (gnu packages protobuf)
    #:use-module (gnu packages time)
    #:use-module (gnu packages check))

(define-public python-cachetools
  (package
    (name "python-cachetools")
    (version "4.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "cachetools" version))
        (sha256
          (base32
            "03mrdnqs8zi9mchpgnacxp68k366jv794724halsq3jg17gf35ip"))))
    (build-system python-build-system)
    (home-page "https://github.com/tkem/cachetools/")
    (synopsis
      "Extensible memoizing collections and decorators")
    (description
      "Extensible memoizing collections and decorators")
    (license license:expat)))

(define-public python-google-auth
  (package
    (name "python-google-auth")
    (version "1.24.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-auth" version))
        (sha256
          (base32
            "0bmdqkyv8k8n6s8dss4zpbcq1cdxwicpb42kwybd02ia85mh43hb"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs
      `(("python-cachetools" ,python-cachetools)
        ("python-pyasn1-modules" ,python-pyasn1-modules)
        ("python-rsa" ,python-rsa)
        ("python-setuptools" ,python-setuptools)
        ("python-six" ,python-six)))
    (home-page
      "https://github.com/googleapis/google-auth-library-python")
    (synopsis "Google Authentication Library")
    (description "Google Authentication Library")
    (license license:asl2.0)))

(define-public python-google-auth-oauthlib
  (package
    (name "python-google-auth-oauthlib")
    (version "0.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-auth-oauthlib" version))
        (sha256
          (base32
            "1nai9k86g7g7w1pxk105dllncgax8nc5hpmk758b3jnqkb1mpdk5"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs
      `(("python-google-auth" ,python-google-auth)
        ("python-requests-oauthlib" ,python-requests-oauthlib)))
    (home-page
      "https://github.com/GoogleCloudPlatform/google-auth-library-python-oauthlib")
    (synopsis "Google Authentication Library")
    (description "Google Authentication Library")
    (license license:asl2.0)))

(define-public python-googleapis-common-protos
  (package
    (name "python-googleapis-common-protos")
    (version "1.52.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "googleapis-common-protos" version))
        (sha256
          (base32
            "0lakcsd35qm5x4visvw6z5f1niasv9a0mjyf2bd98wqi0z41c1sn"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs
      `(("python-protobuf" ,python-protobuf)))
    (home-page
      "https://github.com/googleapis/python-api-common-protos")
    (synopsis "Common protobufs used in Google APIs")
    (description
      "Common protobufs used in Google APIs")
    (license license:asl2.0)))

(define-public python-google-api-core
  (package
    (name "python-google-api-core")
    (version "1.25.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-api-core" version))
        (sha256
          (base32
            "0h140h3xicp9v5370z1xv39iq0x7lm25inaq2azd30c4gg1jw58f"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-google-auth" ,python-google-auth)
        ("python-googleapis-common-protos" ,python-googleapis-common-protos)
        ("python-protobuf" ,python-protobuf)
        ("python-pytz" ,python-pytz)
        ("python-requests" ,python-requests)
        ("python-setuptools" ,python-setuptools)
        ("python-pytest-mock", python-pytest-mock)
        ("python-six" ,python-six)))
    (arguments '(#:tests? #f))
    (home-page
      "https://github.com/googleapis/python-api-core")
    (synopsis "Google API client core library")
    (description "Google API client core library")
    (license license:asl2.0)))

(define-public python-google-auth-httplib2
  (package
    (name "python-google-auth-httplib2")
    (version "0.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-auth-httplib2" version))
        (sha256
          (base32
            "0fdwnx2yd65f5vhnmn39f4xnxac5j6x0pv2p42qifrdi1z32q2cd"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs
      `(("python-google-auth" ,python-google-auth)
        ("python-httplib2" ,python-httplib2)
        ("python-six" ,python-six)))
    (home-page
      "https://github.com/GoogleCloudPlatform/google-auth-library-python-httplib2")
    (synopsis
      "Google Authentication Library: httplib2 transport")
    (description
      "Google Authentication Library: httplib2 transport")
    (license license:asl2.0)))

(define-public python-cachetools
  (package
    (name "python-cachetools")
    (version "4.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "cachetools" version))
        (sha256
          (base32
            "1scjvk1w6r9zv7h6hv514l8yms3dwxfdkap4im6zdkx4gagf4sgl"))))
    (build-system python-build-system)
    (home-page "https://github.com/tkem/cachetools/")
    (synopsis "Extensible memoizing collections and decorators")
    (description "Extensible memoizing collections and decorators")
    (license license:expat)))


(define-public python-google-api-python-client
  (package
    (name "python-google-api-python-client")
    (version "1.12.8")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "google-api-python-client" version))
        (sha256
          (base32
            "1fq89wifa9ymby655is246w5d54ixybffj5vz7lwzhpf8926ifgk"))))
    (build-system python-build-system)
    (arguments '(#:tests? #f))
    (propagated-inputs
      `(("python-google-api-core"
         ,python-google-api-core)
        ("python-google-auth" ,python-google-auth)
        ("python-google-auth-httplib2"
         ,python-google-auth-httplib2)
        ("python-httplib2" ,python-httplib2)
        ("python-six" ,python-six)
        ("python-uritemplate" ,python-uritemplate)))
    (home-page "https://github.com/googleapis/google-api-python-client/")
    (synopsis "Google API Client Library for Python")
    (description
      "Google API Client Library for Python")
    (license license:asl2.0)))
