(define-module (emacs)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build emacs-build-system)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages mail)
)

(define-public emacs-spaceline-all-the-icons
  (package
   (name "emacs-spaceline-all-the-icons")
   (version "1.4.0")
   (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://github.com/domtronn/spaceline-all-the-icons.el")
                   (commit version)))
             (file-name (git-file-name name version))
             (sha256
              (base32
               "186v71d8n1iy73drayyf57pyzlz973q74mazkyvb8w3fj8bb3llm"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-all-the-icons" ,emacs-all-the-icons)
      ("emacs-spaceline" ,emacs-spaceline)
      ("emacs-memoize" ,emacs-memoize)))
   (home-page
    "https://github.com/domtronn/spaceline-all-the-icons.el")
   (synopsis
    "Theme for emacs-spaceline using emacs-all-the-icons")
   (description
    "This package is a theme for `emacs-spaceline' and recreates most of the
segments available in that package using icons from
`emacs-all-the-icons'.  Icon fonts allow for more tailored and detailed
information in the mode line.
")
   (license license:expat)))

(define-public emacs-material-theme
  (package
    (name "emacs-material-theme")
    (version "1.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/cpaulik/emacs-material-theme")
             (commit "V1.3")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1qg59w216iv1yk594xjgazfqx5qhw3jgjqf32pcwwbkcnyj5f6zw"))))
    (build-system emacs-build-system)
    (home-page
     "http://github.com/cpaulik/emacs-material-theme")
    (synopsis
     "Theme for emacs based on the colors of the Google Material Design")
    (description
     "A theme for emacs based on the colors of the Google Material Design.
Requirements: Emacs 24.
")
    (license license:expat)))

(define-public emacs-svg-tag-mode
  (package
   (name "emacs-svg-tag-mode")
   (version "20201129.608")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/svg-tag-mode-"
           version
           ".el"))
     (sha256
      (base32
       "18r0rxh5860rybyw7141b4hdh00w1nxlh5jar68cv0wanwxc3nq3"))))
   (build-system emacs-build-system)
   (home-page
    "https://github.com/rougier/svg-tag-mode")
   (synopsis "Replace keywords with SVG tags")
   (description
    "This minor mode replaces keywords or expressions with SVG rounded
box labels that are fully customizable.
")
   (license license:gpl3)))

(define-public emacs-all-the-icons-ibuffer
  (package
   (name "emacs-all-the-icons-ibuffer")
   (version "1.3.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/seagle0128/all-the-icons-ibuffer")
           (commit "v1.3.0")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "1c1fkkwzxxa90fd5q5x6xlj48p8rhj4bs7h8wxx41w6wsggk2fm2"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-all-the-icons" ,emacs-all-the-icons)))
   (home-page
    "https://github.com/seagle0128/all-the-icons-ibuffer")
   (synopsis
    "Display icons for all buffers in ibuffer")
   (description
    "Uses `emacs-all-the-icons` to display icons in ibuffer buffers.")
   (license license:gpl3)))

(define-public emacs-shrface
  (package
   (name "emacs-shrface")
   (version "2.6.2")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/chenyanming/shrface")
           (commit "2.6.2")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0b0naykw21kgc5z1z8mzczabac9xr1njs2fp5bgk4b32098gkjs0"))))
   (build-system emacs-build-system)
   (propagated-inputs `(("emacs-org" ,emacs-org)))
   (home-page
    "https://github.com/chenyanming/shrface")
   (synopsis
    "Extend shr/eww with org features and analysis capability")
   (description
    "This package extends `shr' / `eww' with org features and analysis capability.
It can be used in `dash-docs', `eww', `nov.el', and `mu4e'.
- Configurable org-like heading faces, headline bullets, item bullets, paragraph
indentation, fill-column, item bullet, versatile hyper
links(http/https/file/mailto/etc) face and so on.
- Browse the internet or local html file with `eww' just like org mode.
- Read dash docsets with `dash-docs' and the beauty of org faces.
- Read epub files with `nov.el' , just like org mode.
- Read html email with `mu4e' , the same reading experience just like org mode
without formatting html to org file.
- Switch/jump the headlines just like org-mode in `eww' and `nov.el' with `imenu'
- Toggle/cycle the headlines just like org-mode in `eww' and `nov.el' with `outline-minor-mode'
and `org-cycle'/`org-shifttab'
- Enable indentation just like org-mode in `eww' and `nov.el' with `org-indent-mode'
- Analysis capability for Researchers:
- Headline analysis: List all headlines with clickable texts.
- URL analysis: List all classified URL with clickable texts.
")
   (license license:gpl3)))

(define-public emacs-mu4e-marker-icons
  (package
   (name "emacs-mu4e-marker-icons")
   (version "20210119.514")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/mu4e-marker-icons-"
           version
           ".el"))
     (sha256
      (base32
       "1fkyldssfxxgp6f8gc0pkdyavl1aj2h6s1xrx5c2938kc51yfjdf"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("mu" ,mu)
      ("emacs-all-the-icons" ,emacs-all-the-icons)))
   (home-page
    "https://github.com/stardiviner/mu4e-marker-icons")
   (synopsis "Display icons for mu4e markers")
   (description
    "Usage (mu4e-marker-icons-mode 1).")
   (license license:gpl3)))
(define-public emacs-enlive
  (package
  (name "emacs-enlive")
  (version "20170725.1417")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "https://melpa.org/packages/enlive-"
             version
             ".el"))
      (sha256
        (base32
          "0ddxh3rc75byrg3xgndqk72glqkgzvx0jkhgqkkcw17p2y658b7z"))))
  (build-system emacs-build-system)
  (home-page "http://github.com/zweifisch/enlive")
  (synopsis
    "query html document with css selectors")
  (description
    "
query html document with css selectors
")
  (license license:gpl3+)))

(define-public emacs-org-books
  (package
  (name "emacs-org-books")
  (version "0.2.21")
  (source
   (origin
    (method git-fetch)
    (uri (git-reference
          (url "https://github.com/lepisma/org-books")
          (commit version)))
    (file-name (git-file-name name version))
    (sha256
     (base32
      "1axzhb9k1i8l9rksk14bb04v4q4mx498f5psnalxwvn0563ngs5r"))))
  (build-system emacs-build-system)
  (propagated-inputs
    `(("emacs-enlive" ,emacs-enlive)
      ("emacs-s" ,emacs-s)
      ("emacs-helm" ,emacs-helm)
      ("emacs-helm-org" ,emacs-helm-org)
      ("emacs-dash" ,emacs-dash)
      ("emacs-org" ,emacs-org)))
  (home-page
    "https://github.com/lepisma/org-books")
  (synopsis
    "Reading list management with Org mode and helm")
  (description
    "org-books.el is a tool for managing reading list in an Org mode file.
This file is not a part of GNU Emacs.
")
  (license license:gpl3)))

(define-public emacs-latex-extra
  (package
   (name "emacs-latex-extra")
   (version "20170817.147")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/latex-extra-"
           version
           ".el"))
     (sha256
      (base32
       "149c5wbmawrn6nnp81p2yx9cpzx2rnzx98bazpi4hadygix7r48a"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-auctex" ,emacs-auctex)))
   (home-page
    "http://github.com/Malabarba/latex-extra")
   (synopsis
    "Adds several useful functionalities to LaTeX-mode.")
   (description
    "
Defines extra commands and keys for LaTeX-mode. To activate (after
installing from melpa) just call
    (add-hook 'LaTeX-mode-hook #'latex-extra-mode)")
   (license license:gpl3)))

(define-public emacs-org-chef
  (package
   (name "emacs-org-chef")
   (version "20200729.2021")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/org-chef-"
           version
           ".tar"))
     (sha256
      (base32
       "1ljq2h2ngpykvqxlkz4dszgdphy094787yzlms06xd7apgfzc9sk"))))
   (build-system emacs-build-system)
   (propagated-inputs `(("emacs-org" ,emacs-org)))
   (home-page "https://github.com/Chobbes/org-chef")
   (synopsis
    "Cookbook and recipe management with org-mode.")
   (description
    "org-chef is a package for managing recipes in org-mode.  One of the
main features is that it can automatically extract recipes from
websites like allrecipes.com
")
   (license license:expat)))

(define-public emacs-doct
  (package
    (name "emacs-doct")
    (version "20210126.310")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://melpa.org/packages/doct-"
             version
             ".el"))
       (sha256
        (base32
         "0f42ld8ky2isd2mixrvifv8biibq98n7bir9n3f7ixrjgwqdjj84"))))
    (build-system emacs-build-system)
    (home-page "https://github.com/progfolio/doct")
    (synopsis
     "DOCT: Declarative Org capture templates")
    (description
     "This package provides an alternative syntax for declaring Org capture
templates. See the doct docstring for more details.
")
   (license license:gpl3)))

(define-public emacs-elfeed-protocol
  (package
   (name "emacs-elfeed-protocol")
   (version "20201013.751")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://melpa.org/packages/elfeed-protocol-"
           version
           ".tar"))
     (sha256
      (base32
       "1igygvljmqqdm1h8cjrxjhjfn9hv1hn4d96zm2ljqd2jjiiqhi4n"))))
   (build-system emacs-build-system)
   (propagated-inputs
    `(("emacs-elfeed" ,emacs-elfeed)))
   (home-page
    "https://github.com/fasheng/elfeed-protocol")
   (synopsis
    "Provide fever/newsblur/owncloud/ttrss protocols for elfeed")
   (description
    "elfeed-protocol provide extra protocols to make self-hosting RSS
readers like Fever, NewsBlur, ownCloud News and Tiny TIny RSS work
with elfeed.  See the README for full documentation.")
   (license license:gpl3)))
