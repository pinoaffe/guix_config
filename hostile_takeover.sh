#!/bin/bash

e2label /dev/sda1 root
dnf update
dnf install xz
wget https://ftp.gnu.org/gnu/guix/guix-binary-1.0.1.x86_64-linux.tar.xz
cd /tmp
tar --warning=no-timestamp -xf ~/guix-binary-1.0.1.x86_64-linux.tar.xz
mv var/guix /var/ && mv gnu /
mkdir -p ~root/.config/guix
ln -sf /var/guix/profiles/per-user/root/current-guix ~root/.config/guix/current
export GUIX_PROFILE="`echo ~root`/.config/guix/current" ;
source $GUIX_PROFILE/etc/profile
groupadd --system guixbuild
for i in `seq -w 1 10`; do
   useradd -g guixbuild -G guixbuild                    -d /var/empty -s `which nologin`             -c "Guix build user $i" --system             guixbuilder$i;
done;
cp ~root/.config/guix/current/lib/systemd/system/guix-daemon.service /etc/systemd/system/
systemctl start guix-daemon && systemctl enable guix-daemon
mkdir -p /usr/local/bin
cd /usr/local/bin
ln -s /var/guix/profiles/per-user/root/current-guix/bin/guix
mkdir -p /usr/local/share/info
cd /usr/local/share/info
for i in /var/guix/profiles/per-user/root/current-guix/share/info/*; do
    ln -s $i;
done
guix archive --authorize < ~root/.config/guix/current/share/guix/ci.guix.gnu.org.pub
# FIXME: I'm pulling a commit that fixes some issues.  When there is a new
# guix release this can be removed.
guix pull --commit=3a695c01d7ee18f30f22df53f3c44dfac04017f1
guix package -i openssl
# FIXME: Just loading the default example from the guix manual here.  This can
# be adapted to whatever base guix deployment you want.

guix system build ~/sigrdrifa.scm
guix system reconfigure ~/sigrdrifa.scm
mv /etc /old-etc
mkdir /etc
cp -r /old-etc/{passwd,group,shadow,gshadow,mtab,guix} /etc/
guix system reconfigure ~/sigrdrifa.scm
echo ". /etc/bashrc" >> /root/.bashrc
echo ". /etc/profile" >> /root/.bashrc
