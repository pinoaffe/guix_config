(use-modules (ice-9 rdelim)
             (ice-9 popen)
             (ice-9 format)
             (ice-9 threads)
             (srfi srfi-1)
             (srfi srfi-2))

(define (get-shell-output . args)
  (let* ((pipe (apply open-pipe* OPEN_READ args))
         (string (read-line pipe)))
    (close-pipe pipe)
    string))

(define (get-number-of-workspaces)
  (string->number
   (get-shell-output "xdotool" "get_num_desktops")))

(define (get-workspace)
  (string->number
   (get-shell-output "xdotool" "get_desktop")))

(define (get-active-workspaces)
  '())

(define (show-workspace n current active)
  (format #f
          "%{A:(workspace ~a):}%{F~a} ~d %{F-}%{A}"
          n
          (cond ((= n current)     "#e67128")
                ((member n active) "#FFFFFF")
                (else              "#606060"))
          n))

(define (workspaces-string)
  (let* ((current (get-workspace))
         (active (get-active-workspaces))
         (all (iota (get-number-of-workspaces))))
    (apply string-append
           (map (lambda (n)
                  (show-workspace n current active))
                all))))

(define (music-string)
  (get-shell-output "ncmpcpp" "--current-song"))

(define (time-string)
  (get-shell-output "date"))

(define battery "BAT1")
(define (battery-string)
  (let* ((battery-dir (string-append "/sys/class/power_supply/"
                                     battery))
         (capacity (string->number
                    (get-shell-output "cat"
                                      (string-append battery-dir "/capacity"))))
         (state (get-shell-output "cat"
                                  (string-append battery-dir "/status"))))
    capacity))

(define-syntax while-let
  (syntax-rules ()
    ((while-let condition ((divisor varname expr) ...) exp exp* ...)
     (let ((counter 0)
           (varname expr) ...)
       (while condition
         (when (= 0 (remainder counter divisor))
           (set! varname expr)) ...
         exp
         exp* ...)))))

(define-syntax macro-apply
  (syntax-rules ()
    ((macro-apply macro exp* ...)
     (primitive-eval (list macro 'exp* ...)))))

(let ((lemonbar (open-pipe* OPEN_WRITE "lemonbar" "-a" "30")))
  (with-output-to-port lemonbar
    (lambda ()
      (while-let #t
                 ((1 workspaces (workspaces-string))
                  (1 time (time-string))
                  (10 battery (battery-string))
                  (100 music (music-string)))
                 (display (format #f
                                  "%{l} ~a | ~a  %{c} ~a %{r} ~a \n"
                                  battery
                                  music
                                  workspaces
                                  time))
                 (force-output)
                 (usleep 100000))))
  (close-port lemonbar))
