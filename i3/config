set $mod Mod4
font pango:monospace 8

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod
# start a terminal
bindsym $mod+Return exec st
#bindsym $mod+Return exec i3-sensible-terminal
# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
#bindsym $mod+space exec dmenu_run -nb "#3b2040" -nf "#ffffff" -sb "#563d64" -sf "#ffffff" -p command
bindsym $mod+space exec ~/scripts/themed_dmenu_run.sh -p command
bindsym $mod+q exec quickswitch.py
bindsym $mod+p exec ~/scripts/dmenu_pass.sh
bindsym $mod+b exec ~/scripts/dmenu_books.sh
bindsym $mod+u exec ~/scripts/dmenu_uni_books.sh
bindsym $mod+d exec ~/scripts/dmenu_documentation.sh
bindsym $mod+s exec ~/scripts/dmenu_schedules.sh
bindsym $mod+g exec ~/scripts/dmenu_environment.sh

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+o split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
#bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+m layout toggle tabbed splith
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
# bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

bindsym XF86AudioRaiseVolume  exec --no-startup-id pactl set-sink-volume 0 +5%
bindsym XF86AudioLowerVolume  exec --no-startup-id pactl set-sink-volume 0 -5%
bindsym XF86AudioMute         exec --no-startup-id pactl set-sink-mute 0 toggle
bindsym XF86AudioMicMute      exec --no-startup-id pactl set-source-mute 1 toggle

bindsym XF86MonBrightnessUp   exec --no-startup-id xbacklight -inc 20
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 20

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1  "01:web"
set $ws2  "02:ter"
set $ws3  "03:rem"
set $ws4  "04:med"
set $ws5  "05:cad"
set $ws6  "06:soc"
set $ws7  "07:lng"
set $ws8  "08:man"
set $ws9  "09:etc"
set $ws10 "10:log"
set $ws11 "11:uni"
set $ws12 "12:synth"
set $ws13 "13:wood"
set $ws14 "14:14"
set $ws15 "15:15"
set $ws16 "16:16"
set $ws17 "17:17"
set $ws18 "18:18"
set $ws19 "19:19"
set $ws20 "20:20"
set $ws21 "20:21"
set $ws22 "20:22"


# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10
bindsym $mod+F1 workspace $ws11
bindsym $mod+F2 workspace $ws12
bindsym $mod+F3 workspace $ws13
bindsym $mod+F4 workspace $ws14
bindsym $mod+F5 workspace $ws15
bindsym $mod+F6 workspace $ws16
bindsym $mod+F7 workspace $ws17
bindsym $mod+F8 workspace $ws18
bindsym $mod+F9 workspace $ws19
bindsym $mod+F10 workspace $ws20
bindsym $mod+F11 workspace $ws21
bindsym $mod+F12 workspace $ws22

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10
bindsym $mod+Shift+F1 move container to workspace $ws11
bindsym $mod+Shift+F2 move container to workspace $ws12
bindsym $mod+Shift+F3 move container to workspace $ws13
bindsym $mod+Shift+F4 move container to workspace $ws14
bindsym $mod+Shift+F5 move container to workspace $ws15
bindsym $mod+Shift+F6 move container to workspace $ws16
bindsym $mod+Shift+F7 move container to workspace $ws17
bindsym $mod+Shift+F8 move container to workspace $ws18
bindsym $mod+Shift+F9 move container to workspace $ws19
bindsym $mod+Shift+F10 move container to workspace $ws20
bindsym $mod+Shift+F11 move container to workspace $ws21
bindsym $mod+Shift+F12 move container to workspace $ws22

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'Exit i3?' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
	# Pressing left will shrink the window’s width.
	# Pressing right will grow the window’s width.
	# Pressing up will shrink the window’s height.
	# Pressing down will grow the window’s height.
	bindsym h resize shrink width  10 px or 10 ppt
	bindsym j resize grow   height 10 px or 10 ppt
	bindsym k resize shrink height 10 px or 10 ppt
	bindsym l resize grow   width  10 px or 10 ppt

	# same bindings, but for the arrow keys
	bindsym Left  resize shrink width  10 px or 10 ppt
	bindsym Down  resize grow   height 10 px or 10 ppt
	bindsym Up    resize shrink height 10 px or 10 ppt
	bindsym Right resize grow   width  10 px or 10 ppt

	# back to normal: Enter or Escape or $mod+r
	bindsym Return mode "default"
	bindsym Escape mode "default"
	bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

bar {
	font pango:monospace 9
	status_command i3status
	colors {
		#background #6688bb
		#separator #ffff00
		#statusline #2f2262
		#focused_workspace  #563d64 #563d64 #ffffff
		#active_workspace   #6688bb #6688bb #ffffff
		#inactive_workspace #6688bb #6688bb #ffffff
		#urgent_workspace   #563d64 #563d64 #ffffff
		#binding_mode       #563d64 #563d64 #ffffff

		background #533a2a
		separator #ffff00
		statusline #decba2

		focused_workspace  #d37a15 #d37a15 #decba2
		urgent_workspace   #d37a15 #d37a15 #decba2
		binding_mode       #d37a15 #d37a15 #decba2
		active_workspace   #8e4823 #8e4823 #decba2
		inactive_workspace #8e4823 #8e4823 #decba2
	}
}


font pango:monospace 9
## class                 border  backgr. text    indicator child_border
#client.focused          #563d64 #563d64 #ffffff #563d64   #563d64
#client.focused_inactive #563d64 #563d64 #ffffff #563d64   #563d64
#client.urgent           #563d64 #563d64 #ffffff #563d64   #563d64
#
#client.unfocused        #3b2040 #3b2040 #ffffff #3b2040   #3b2040
#client.placeholder      #38203e #38203e #ffffff #38203e   #38203e
#
#client.background       #563d64

# class                 border  backgr. text    indicator child_border
#client.focused          #bb3e36 #bb3e36 #ffffff #bb3e36   #bb3e36
client.focused          #872d29 #872d29 #ffffff #872d29   #872d29
client.focused_inactive #872d29 #872d29 #ffffff #872d29   #872d29
client.urgent           #872d29 #872d29 #ffffff #872d29   #872d29

#client.unfocused        #7a201c #7a201c #ffffff #7a201c   #7a201c
client.unfocused        #6d130f #6d130f #ffffff #6d130f   #6d130f
client.placeholder      #38203e #38203e #ffffff #38203e   #38203e

client.background       #563d64

exec --no-startup-id feh --bg-fill ~/wallpaper.jpg
exec --no-startup-id compton
exec --no-startup-id setxkbmap -option caps:escape
