
cd /data/uni/books/
find -name "*.pdf"\
| sort \
| bash ~/scripts/themed_dmenu.sh -p "uni book" -l 6 \
| xargs -r zathura
