
cd
find /data/config/manifests/ -type f -name "*.scm"\
| xargs -n1 basename \
| sort \
| sed 's/\..*//' \
| bash ~/scripts/themed_dmenu.sh -p environment -l 6 \
| sed 's;^;/data/config/manifests/;;s;$;.scm;' \
| xargs -r st guix environment -m
