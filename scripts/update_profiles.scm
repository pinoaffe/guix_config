(use-modules (guix)
             (guix scripts)
             (guix scripts package)
             (guix scripts environment)
             (srfi srfi-1)
             (srfi srfi-2))

(define manifest-prefix "/home/pino/data/config/manifests")
(define profile-prefix "/home/pino/profiles")

(define (read-file-contents file)
  (call-with-input-file file read))

(define core-manifests
  (read-file-contents (format "~a/core.scm" manifest-prefix)))

(define optional-manifests
  (read-file-contents (format "~a/optional.scm" manifest-prefix)))

(define all-manifests
  (append core-manifests optional-manifests))

(define (manifest->manifest-file manifest)
  (string-append manifest-prefix "/" manifest ".scm"))

(define (manifest->profile-dir manifest)
  (string-append profile-prefix "/" manifest))

(define (manifest->profile-file manifest)
  (string-append profile-prefix "/" manifest "/" manifest))

(define (ensure-directory dir)
  (system* "mkdir" "-p" dir))

(define (ensure-profile-directory manifest)
  (ensure-directory (manifest->profile-dir manifest)))

(define (ensure-profile-directories)
  (for-each ensure-profile-directory all-manifests))

(define (manifest->manifest-file manifest)
  (string-append manifest-prefix manifest ".scm"))

(define (update-profile manifest)
  (ensure-profile-directory manifest)
  (guix-package "-m" (manifest->manifest-file manifest)
                "-p" (manifest->profile-file manifest)))

(define (update-profiles manifests)
  (for-each update-profile
            manifests))

(define (manifests->environment-variables . manifests)
  (apply guix-package "--search-paths"
         (fold (lambda (element accumulator)
                 (cons "-p" (cons element accumulator)))
               '()
               (map manifest->profile-file
                    manifests))))

(update-profiles all-manifests)
