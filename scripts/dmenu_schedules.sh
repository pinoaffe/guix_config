
find /data/planning/schedules/ -name "*.pdf"\
| sed 's;/data/planning/schedules/;;'\
| sort \
| bash ~/scripts/themed_dmenu.sh -p schedule -l 6 \
| sed 's;^;/data/planning/schedules/;'\
| xargs -r zathura
