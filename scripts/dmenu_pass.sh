xdotool type "$(find ~/.password-store -name "*.gpg" \
| sed 's/\.gpg$//' | sed 's/^.*\.password-store\///' \
| bash ~/scripts/themed_dmenu.sh -p pass -l 6 \
| xargs -r pass \
| tr -d '\n')"
