
cd /data/ebooks/
find -name "*.pdf"\
| sort \
| bash ~/scripts/themed_dmenu.sh -p book -l 6 \
| xargs -r zathura
