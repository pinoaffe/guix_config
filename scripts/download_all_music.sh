xclip -o -sel clip \
    | dos2unix \
    | sort -u
    | xargs -n1 youtube-dl -i -f m4a
