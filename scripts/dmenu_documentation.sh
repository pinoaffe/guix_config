
find -L /data/documentation/ -name "*.pdf"\
| sed 's;/data/documentation/;;'\
| sort\
| bash ~/scripts/themed_dmenu.sh -p documentation -l 6 \
| sed 's;^;/data/documentation/;'\
| xargs -r zathura
